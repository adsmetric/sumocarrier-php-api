<?php

namespace SumoCarrier;

/**
 * Created by PhpStorm.
 * User: allenlinatoc
 * Date: 17/06/2016
 * Time: 11:48 AM
 */
class Server {

    private $server;


    public function __construct($server_url) {
        $this->server = rtrim($server_url, '/');
    }


    /**
     * @return mixed
     */
    public function getServer() {
        return $this->server;
    }

    public function transaction_id_url() {
        return $this->getServer() . '/sumoapi/generate_id';
    }

    public function subscribe_url() {
        return $this->getServer() . '/notify/subscribe';
    }


    public function unsubscribe_url() {
        return $this->getServer() . '/notify/unsubscribe';
    }


    public function renew_url() {
        return $this->getServer() . '/notify/renew';
    }


    /**
     * Initialise new adapter instance to a SumoCarrier server
     *
     * @param string $server_url        The URL of SumoCarrier server
     * @return Server
     */
    static public function init($server_url) {
        return new self($server_url);
    }




}