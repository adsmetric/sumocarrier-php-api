## SumoCarrier API

Full-blown documentation and samples will soon be migrated here.

Changelog (2016-07-28):

* You can now create ``APICall`` instance through 

> ``APICall::create($transaction)``

* Fixed ``Transaction::create()`` method with arguments aligned with its native constructor:

> ``$server[ , $api_key = null, $transaction_id = null ]``

* Added method chaining for setters of `APICall` and `Transaction`:

> `$api->setOfferID(12)
        ->setCarrier(12)
        ->setMsisdn(‘61410123456’)
        ->set…`

* Added the following APICall properties (dedicated for ``/subscribe`` method):

> ``$carrier`` — ``getCarrier()``, ``setCarrier($carrier)``

>``$msisdn`` — ``getMsisdn()``, ``setMsisdn($msisdn)``

* Added ``APICall::assign($data)`` method, where ``$data`` is an array of multiple assignments in compliance with APICall setters (**case-insensitive**). Concept similar to Laravel.

> Example:
```
$api->asssign(array(
    'user_id' => 'user123',
    'carrier' => 2,
    'msisdn' => '61410123456'
));
```
