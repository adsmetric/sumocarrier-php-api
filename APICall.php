<?php

namespace SumoCarrier;

class APICall {

    const CARRIER_OPTUS = 1;
    const CARRIER_TELSTRA = 2;
    const CARRIER_VODAFONE = 3;
    const CARRIER_UNKNOWN = 4;
    const EVENT_CANCEL = 'cancel';
    const EVENT_CARRIER_AUTH = 'carrier_auth';
    const EVENT_SUBSCRIBE = 'subscribe';

    public $offer_id;
    public $user_id;
    public $status;
    public $event_type;
    public $carrier;
    public $msisdn;


    private $last_response = null;

    /** @var Transaction */
    public $transaction;


    public function __construct(Transaction $transaction) {
        $this->transaction = $transaction;
    }


    public function event() {
        $api_key = $this->getTransaction()->getAPIKey();
        $offer_id = $this->offer_id;
        $transaction_id = $this->transaction->getID();
        $type = $this->event_type;
        $carrier = $this->carrier;

        $data = compact('api_key', 'offer_id', 'transaction_id', 'type', 'carrier');

        $response = $this->getTransaction()->request($this->getTransaction()->getServer()->renew_url(), $data);
        if (is_array($response)) {
            $this->last_response = $response;
        }

        return $response;
    }


    public function renew() {
        $api_key = $this->getTransaction()->getAPIKey();
        $transaction_id = $this->getTransaction()->getID();

        $data = compact('api_key', 'transaction_id');

        $response = $this->getTransaction()->request($this->getTransaction()->getServer()->renew_url(), $data);
        if (is_array($response)) {
            $this->last_response = $response;
        }

        return $response;
    }


    public function subscribe($force = false) {
        if (!empty($this->last_response)) {
            if (!$force) {
                return $this->last_response;
            }
        }

        $api_key = $this->getTransaction()->getAPIKey();
        $offer_id = $this->getOfferId();
        $user_id = $this->getUserId();
        $status = $this->getStatus();
        $transaction_id = $this->getTransaction()->getID();
        $msisdn = $this->getMsisdn();
        $carrier = $this->getCarrier();

        $data = compact('api_key', 'offer_id', 'user_id', 'status', 'transaction_id', 'msisdn', 'carrier');
        if (empty($data['status'])) {
            $data['status'] = '0';
        }

        $response = $this->getTransaction()->request($this->getTransaction()->getServer()->subscribe_url(), $data);
        if (is_array($response)) {
            $this->last_response = $response;
        }
        return $response;
    }

    public function unsubscribe() {
        $api_key = $this->getTransaction()->getAPIKey();
        $transaction_id = $this->getTransaction()->getID();

        $data = compact('api_key', 'transaction_id');
        $response = $this->getTransaction()->request($this->getTransaction()->getServer()->unsubscribe_url(), $data);
        if (is_array($response)) {
            $this->last_response = $response;
        }
        return $response;
    }

    /**
     * @return mixed
     */
    public function getOfferId() {
        return $this->offer_id;
    }

    /**
     * @param mixed $offer_id
     *
     * @return $this
     */
    public function setOfferId($offer_id) {
        $this->offer_id = $offer_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMsisdn() {
        return $this->msisdn;
    }

    /**
     * @param mixed $msisdn
     *
     * @return $this
     */
    public function setMsisdn($msisdn) {
        $this->msisdn = $msisdn;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarrier() {
        return $this->carrier;
    }

    /**
     * @param mixed $carrier
     *
     * @return $this
     */
    public function setCarrier($carrier) {
        $this->carrier = $carrier;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     *
     * @return $this
     */
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return $this
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Transaction
     */
    public function getTransaction() {
        return $this->transaction;
    }

    /**
     * @param Transaction $transaction
     *
     * @return $this
     */
    public function setTransaction($transaction) {
        $this->transaction = $transaction;
        return $this;
    }


    public function assign($data) {
        static $methods;
        if (empty($methods)) {
            $methods = get_class_methods(get_class($this));
        }

        foreach (array_keys($data) as $key) {
            $key = trim($key);

            $setter = null;

            foreach ($methods as $method) {
                if (strcasecmp(strtolower('set'.$key), $method) == 0) {
                    $setter = $method;
                    break;
                }
            }

            // If not resolved, skip this assignment
            if (empty($setter)) {
                continue;
            }

            $value = $data[$key];
            $this->{$setter}($value);
        }
    }


    /**
     * Static way to Create new API call
     *
     * @param Transaction $transaction
     * @return APICall
     */
    static public function create(Transaction $transaction) {
        return new APICall($transaction);
    }



}